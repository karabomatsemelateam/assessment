package com.example.karabo.weather.View;

import android.arch.lifecycle.Observer;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.karabo.weather.Model.Forecast;
import com.example.karabo.weather.Model.CurrentWeather;
import com.example.karabo.weather.R;
import com.example.karabo.weather.Util.Constants;
import com.example.karabo.weather.Util.ForecastAdapter;
import com.example.karabo.weather.ViewModel.WeatherViewModel;

public class WeatherActivity extends AppCompatActivity {

    private WeatherViewModel viewModel;
    private Observer<CurrentWeather> currentWeatherObserver;
    private Observer<Forecast> weatherForecastObserver;

    //UI Views : RelativeLayouts
    private RelativeLayout currentWeatherTopLayout;
    private RelativeLayout currentWeatherBottomLayout;
    private RelativeLayout forecastWeatherLayout;

    //UI Views : TextViews
    private TextView currentTemperatureTextView;
    private TextView currentWeatherTextView;
    private TextView minTextView;
    private TextView currentTextView;
    private TextView maxTextView;

    //RecyclerView
    private RecyclerView forecastRecyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        initialize();
        setupWeatherInformation();
    }

    public void initialize() {
        //Initialize
        currentWeatherTopLayout = findViewById(R.id.current_weather_top_layout);
        currentWeatherBottomLayout = findViewById(R.id.current_weather_bottom_layout);
        forecastWeatherLayout = findViewById(R.id.forecast_weather_layout);
        currentTemperatureTextView = findViewById(R.id.current_temperature_textView);
        currentWeatherTextView = findViewById(R.id.current_weather_textView);
        minTextView = findViewById(R.id.min_textView);
        currentTextView = findViewById(R.id.current_textView);
        maxTextView = findViewById(R.id.max_textView);
        forecastRecyclerView = findViewById(R.id.forecast_recyclerView);
        layoutManager = new LinearLayoutManager(this);
        forecastRecyclerView.setLayoutManager(layoutManager);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                //Permission denied
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    boolean checkedDoNotAskAgain = shouldShowRequestPermissionRationale( permissions[0] );
                    if (checkedDoNotAskAgain) {
                        //User checked "never ask again"
                        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
                    } else {
                        Toast.makeText(this, "You checked don't ask for location permission, the app wont get your location until you uninstall and reinstall.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
                }

            } else {
                setupWeatherInformation();
            }
        }
    }

    private void setupWeatherInformation() {
        //Initialize viewModel
        viewModel = new WeatherViewModel(this);

        // Observe currentWeather
        currentWeatherObserver = new Observer<CurrentWeather>() {

            @Override
            public void onChanged(@Nullable CurrentWeather currentWeather) {
                setupCurrentWeather(currentWeather);
            }
        };

        // Observe weatherForecast
        weatherForecastObserver = new Observer<Forecast>() {

            @Override
            public void onChanged(@Nullable Forecast forecast) {
                setupWeatherForecast(forecast);
            }
        };

        // Initialize observers
        viewModel.getCurrentWeather().observe(this, currentWeatherObserver);
        viewModel.getWeatherForecast().observe(this, weatherForecastObserver);
    }

    private void setupCurrentWeather(CurrentWeather currentWeather) {
        if (currentWeather != null) {
            currentTemperatureTextView.setText(String.valueOf((int) currentWeather.getMain().getTemp()) + (char) 0x00B0);
            currentTextView.setText(String.valueOf((int) currentWeather.getMain().getTemp()) + (char) 0x00B0);
            minTextView.setText(String.valueOf((int) currentWeather.getMain().getTemp_min()) + (char) 0x00B0);
            maxTextView.setText(String.valueOf((int) currentWeather.getMain().getTemp_max()) + (char) 0x00B0);
            currentWeatherTextView.setText(viewModel.getWeatherDescription(currentWeather.getWeather()));
            currentWeatherTopLayout.setBackgroundDrawable(ContextCompat.getDrawable(this,viewModel.getBackgroundImage(currentWeather.getWeather())));
            currentWeatherBottomLayout.setBackgroundColor(ContextCompat.getColor(this, viewModel.getBackgroundColor(currentWeather.getWeather())));
            forecastWeatherLayout.setBackgroundColor(ContextCompat.getColor(this, viewModel.getBackgroundColor(currentWeather.getWeather())));
        }
    }

    private void setupWeatherForecast(Forecast forecast) {
        if (forecast != null) {
            adapter = new ForecastAdapter(this, viewModel.filterOutDuplicateDates(forecast.getList()));
            forecastRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unSubscribe to avoid memory leaks
        viewModel.disposable.dispose();
    }

}


