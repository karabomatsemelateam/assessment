package com.example.karabo.weather.Util;

import android.content.Context;
import android.os.AsyncTask;
import com.example.karabo.weather.ViewModel.WeatherViewModel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpManager {

    public static class GetRequest extends AsyncTask<String, String, String> {

        HttpURLConnection urlConnection = null;
        private TaskCompleted mCallback;
        private WeatherViewModel.WeatherType mWeatherType;

        public GetRequest(Context context, WeatherViewModel.WeatherType weatherType) {
            mCallback = (TaskCompleted) context;
            mWeatherType = weatherType;
        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;
            String responseData = null;

            try {

                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    responseData = convertInputStreamToString(urlConnection.getInputStream());
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseData;
        }

        @Override
        protected void onPostExecute(String responseData) {
            super.onPostExecute(responseData);
            mCallback.onTaskComplete(mWeatherType, responseData);
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
