package com.example.karabo.weather.Util;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.karabo.weather.Model.Forecast;
import com.example.karabo.weather.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {

    private List<Forecast.List> fiveDaysForecast;
    private Context context;

    public ForecastAdapter(Context context, List<Forecast.List> forecast) {
        this.context = context;
        fiveDaysForecast = forecast;
    }
    @NonNull
    @Override
    public ForecastAdapter.ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.forecast_weather_item, viewGroup,false);
        return new ForecastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder forecastViewHolder, int position) {
        forecastViewHolder.bindDataItem(fiveDaysForecast.get(position));
    }

    @Override
    public int getItemCount() {
        return fiveDaysForecast.size();
    }

    class ForecastViewHolder extends RecyclerView.ViewHolder {

        private TextView day;
        private ImageView icon;
        private TextView temperature;

        public ForecastViewHolder(@NonNull View itemView) {
            super(itemView);
            day = itemView.findViewById(R.id.forecast_day);
            icon = itemView.findViewById(R.id.forecast_weather_icon);
            temperature = itemView.findViewById(R.id.forecast_weather_temperature);
        }

        public void bindDataItem(Forecast.List weather) {
            day.setText(weather.getDt_txt());
            icon.setBackgroundDrawable(ContextCompat.getDrawable(context, getIcon(weather)));
            temperature.setText(String.format("%s%s", String.valueOf((int) weather.getMain().getTemp()), (char) 0x00B0));
        }


        public int getIcon(Forecast.List forecastWeather) {
            int id = 0;
            for (Forecast.Weather weather: forecastWeather.getWeather()) {
                if (weather.getMain().toLowerCase().contains("rain")) {
                    id = R.drawable.rain;
                } else if (weather.getMain().toLowerCase().contains("cloud")) {
                    id = R.drawable.partlysunny; // no cloudy icon provided
                } else if (weather.getMain().toLowerCase().contains("sun")) {
                    id = R.drawable.partlysunny;
                } else if (weather.getMain().toLowerCase().contains("clear")) {
                    id = R.drawable.clear;
                } else if (weather.getMain().toLowerCase().contains("thunderstorm")) {
                    id = R.drawable.rain; // no thunderstorm icon provided
                } else {
                    id = R.drawable.clear; // default to clear
                }
            }
            return id;
        }
    }
}


