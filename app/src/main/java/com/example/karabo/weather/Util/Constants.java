package com.example.karabo.weather.Util;

public class Constants {
    public static String URL_CURRENT_WEATHER = "http://api.openweathermap.org/data/2.5/weather?";
    public static String URL_FORECAST_WEATHER = "http://api.openweathermap.org/data/2.5/forecast?";
    public static String APP_ID = "bde2c3334e998ecee7067560994e7ef9";
    public static int LOCATION_PERMISSION_REQUEST_CODE = 1;
}
