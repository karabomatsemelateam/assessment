package com.example.karabo.weather.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Forecast {

    @Expose
    @SerializedName("list")
    private java.util.List<List> list;
    @Expose
    @SerializedName("message")
    private double message;

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public java.util.List<List> getList() { return list; }

    public static class List {
        @Expose
        @SerializedName("dt_txt")
        private String dt_txt;
        @Expose
        @SerializedName("weather")
        private java.util.List<Weather> weather;
        @Expose
        @SerializedName("main")
        private Main main;
        @Expose
        @SerializedName("dt")
        private int dt;

        public String getDt_txt() {
            return dt_txt;
        }

        public void setDt_txt(String dt_txt) {
            this.dt_txt = dt_txt;
        }

        public java.util.List<Weather> getWeather() {
            return weather;
        }

        public void setWeather(java.util.List<Weather> weather) {
            this.weather = weather;
        }

        public Main getMain() {
            return main;
        }

    }

    public static class Weather {
        @Expose
        @SerializedName("icon")
        private String icon;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("main")
        private String main;
        @Expose
        @SerializedName("id")
        private int id;

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMain() {
            return main;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Main {
        @Expose
        @SerializedName("temp")
        private double temp;

        public double getTemp() {
            return temp;
        }
    }

}
