package com.example.karabo.weather.Model;

public class Coordinates {
    public float latitude;
    public float longitude;

    public Coordinates(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
