package com.example.karabo.weather.Util;

import com.example.karabo.weather.ViewModel.WeatherViewModel;

/**
 * Sends a callback after AsyncTask is completed
 */
public interface TaskCompleted  {
    void onTaskComplete(WeatherViewModel.WeatherType weatherType, String result);
}
