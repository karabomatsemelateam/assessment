package com.example.karabo.weather.Util;

import android.content.Context;
import android.net.NetworkInfo;

public class ConnectivityManager {

    /**
     * Check if there is any connectivity
     * @param context
     * @return
     */
    public static boolean isConnected(Context context){
        NetworkInfo info = ConnectivityManager.getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Get the network info
     * @param context
     * @return
     */
    public static NetworkInfo getNetworkInfo(Context context){
        android.net.ConnectivityManager cm = (android.net.ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

}
