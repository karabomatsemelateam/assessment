package com.example.karabo.weather.ViewModel;

import android.app.AlertDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import com.example.karabo.weather.Model.Coordinates;
import com.example.karabo.weather.Model.Forecast;
import com.example.karabo.weather.Model.CurrentWeather;
import com.example.karabo.weather.R;
import com.example.karabo.weather.Util.ConnectivityManager;
import com.example.karabo.weather.Util.Constants;
import com.example.karabo.weather.Util.HttpManager;
import com.example.karabo.weather.Util.LocationHelper;
import com.example.karabo.weather.Util.RxBus;
import com.example.karabo.weather.Util.TaskCompleted;
import com.example.karabo.weather.View.WeatherActivity;
import com.google.gson.GsonBuilder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

public class WeatherViewModel extends WeatherActivity implements TaskCompleted {

    private Context context;
    public Disposable disposable;
    private MutableLiveData<CurrentWeather> currentWeather = new MutableLiveData<>();
    private MutableLiveData<Forecast> weatherForecast = new MutableLiveData<>();

    public WeatherViewModel(Context context) {
        this.context = context;
        getUserCoordinate();
    }

    public enum WeatherType {
        CURRENT,
        FORECAST
    }

    private void getUserCoordinate() {
        new LocationHelper(context);
        disposable = RxBus.getSubject()
                .subscribeWith(new DisposableObserver<Object>() {
                    @Override
                    public void onNext(Object object) {
                        if (ConnectivityManager.isConnected(context)) {
                            fetchCurrentWeatherAndForecast((Coordinates) object);
                        } else {
                            noConnectionAlertDialog();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {}

                    @Override
                    public void onComplete() {}
                });
    }

    private void fetchCurrentWeatherAndForecast(Coordinates coordinates) {
        String urlCurrentWeather = Constants.URL_CURRENT_WEATHER +"lat=" + coordinates.latitude +"&lon="+ coordinates.longitude + "&appid=" + Constants.APP_ID + "&units=metric";
        String urlForecastWeather = Constants.URL_FORECAST_WEATHER +"lat=" + coordinates.latitude +"&lon="+ coordinates.longitude + "&appid=" + Constants.APP_ID + "&units=metric";
        new HttpManager.GetRequest(this, WeatherType.CURRENT).execute(urlCurrentWeather);
        new HttpManager.GetRequest(this, WeatherType.FORECAST).execute(urlForecastWeather);
    }

    @Override
    public void onTaskComplete(WeatherType weatherType,String response) {
        if (weatherType == WeatherType.CURRENT){
            CurrentWeather weather = new GsonBuilder().create().fromJson(response, CurrentWeather.class);
            currentWeather.setValue(weather);
        } else if (weatherType == WeatherType.FORECAST) {
            Forecast forecast = new GsonBuilder().create().fromJson(response, Forecast.class);
            weatherForecast.setValue(forecast);
        }
    }

    public LiveData<CurrentWeather> getCurrentWeather() {
        return currentWeather;
    }

    public LiveData<Forecast> getWeatherForecast() {
        return weatherForecast;
    }

    public String getWeatherDescription(List<CurrentWeather.Weather> currentWeather) {
        String weatherType = "";
        for (CurrentWeather.Weather weather: currentWeather) {
            weatherType = weather.getMain();
        }
        return  weatherType;
    }

    public int getBackgroundImage(List<CurrentWeather.Weather> currentWeather) {
        int id = 0;
        for (CurrentWeather.Weather weather: currentWeather) {
            if (weather.getMain().toLowerCase().contains("rain")) {
                id = R.drawable.rainy;
            } else if (weather.getMain().toLowerCase().contains("cloud") || weather.getMain().toLowerCase().contains("mist")) {
                id = R.drawable.cloudy;
            } else if (weather.getMain().toLowerCase().contains("sun")) {
                id = R.drawable.sunny;
            } else if (weather.getMain().toLowerCase().contains("clear")) {
                id = R.drawable.sunny;
            } else if (weather.getMain().toLowerCase().contains("thunderstorm")){
                id = R.drawable.rainy;
            } else {
                id = R.drawable.sunny;
            }
        }
        return id;
    }

    public int getBackgroundColor(List<CurrentWeather.Weather> currentWeather) {
        int id = 0;
        for (CurrentWeather.Weather weather: currentWeather) {
            if (weather.getMain().toLowerCase().contains("rain")) {
                id = R.color.colorRainy;
            } else if (weather.getMain().toLowerCase().contains("cloud") || weather.getMain().toLowerCase().contains("mist")) {
                id = R.color.colorCloudy;
            } else if (weather.getMain().toLowerCase().contains("sun")) {
                id = R.color.colorSunny;
            } else if (weather.getMain().toLowerCase().contains("clear")) {
                id = R.color.colorSunny;
            } else if (weather.getMain().toLowerCase().contains("thunderstorm")){
                id = R.color.colorRainy;
            } else {
                id = R.color.colorSunny;
            }
        }
        return id;
    }

    public List<Forecast.List> filterOutDuplicateDates(List<Forecast.List> list) {
        List<Forecast.List> newList = new ArrayList<>();
        for (Forecast.List item: list) {
            item.setDt_txt(convertDateToDay(item.getDt_txt()));
        }

        for (Forecast.List item: list) {
            boolean exists = false;
            for (Forecast.List newItem: newList) {
                if (item.getDt_txt().contains(newItem.getDt_txt())) {
                    exists = true;
                }
            }

            if (!exists) {
                newList.add(item);
            }

            //We only need to show 5 day forecast
            if (newList.size() == 5) {
                return newList;
            }
        }
        return newList;
    }

    public String convertDateToDay(String date) {
        Date myDate = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
        try {
            myDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dateFormat.applyPattern("EEEE");
        String dayOfWeek = dateFormat.format(myDate);
        return dayOfWeek;
    }

    private void noConnectionAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("NO INTERNET");
        builder.setMessage("Please Check Your Internet Connection.");
        builder.setPositiveButton("Enable INTERNET", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                context.startActivity(intent);
            }
        });
        builder.show();
    }

}
